from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QProgressBar
from PyQt5.QtCore import pyqtSlot, QTimeLine
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
		

    def initUI(self):        
        self.setWindowTitle('QProgressBar demo')        
       
        self.timerButton = QPushButton("Start", self)
        self.timerButton.clicked.connect(self.timerStart)

        self.progressBar = QProgressBar(self)
        self.progressBar.setGeometry(10, 20, 290, 25)                

        self.timerObject = QTimeLine(2000, self)
        self.timerObject.setFrameRange(0, 100)
        self.timerObject.finished.connect(lambda: self.timerButton.setText("Finished"))
        self.timerObject.frameChanged.connect(self.progressBar.setValue)

        self.timerButton.move(110,150)        
        self.progressBar.move(10,100)

        self.resize(300, 300)
        self.show()

    @pyqtSlot()
    def timerStart(self):
        if (self.timerObject.state() == QTimeLine.Running):
            self.timerObject.stop()
            self.timerButton.setText("Resume")
        else:            
            self.timerButton.setText("Pause")
            self.timerObject.resume()

if __name__ == '__main__':    
    qApp = QApplication(sys.argv)
    w = MainWindow()
    sys.exit(qApp.exec_())
